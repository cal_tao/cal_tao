import "dart:io";

class Cal {
  dynamic sum = 0;
  double getSum() {
    return this.sum;
  }

  int additionNumber(int n1, int n2) {
    int answer = 0;
    answer = n1 + n2;
    sum += answer;
    return sum;
  }

  int subtractionNumber(int n1, int n2) {
    int answer = 0;
    answer = n1 - n2;
    sum += answer;
    return sum;
  }

  int multiplicationNumber(int n1, int n2) {
    int answer = 0;
    sum += answer;
    return sum;
  }

  double divisionNumber(int n1, int n2) {
    double answer = 0;
    answer = n1 / n2;
    sum += answer;
    return sum;
  }

  double exponentNumber(int n1, int n2) {
    double answer = 1;
    for (int i = 0; i < n2; i++) {
      answer *= n1;
    }
    sum += answer;
    return sum;
  }

  void clearSum() {
    sum = 0;
  }

  void showMenu() {
    print("menu  "); // choose menu
    print("addition       :   press 1");
    print("subtraction    :   press 2");
    print("multiplication :   press 3");
    print("division       :   press 4");
    print("exponent       :   press 5");
    print("clear          :   press 0");
    print("Exit           :   press -1");
  }
}

void main(List<String> arguments) {
  Cal cal = new Cal();

  // print("Enter n1 : ");
  // int n1 = int.parse(stdin.readLineSync()!);
  // print("Enter n2 : ");
  // int n2 = int.parse(stdin.readLineSync()!);

  // print ("Addition (n1 + n2) :");
  // print(cal.additionNumber(n1, n2));
  cal.showMenu();
  int menu = int.parse(stdin.readLineSync()!);
  bool checkLoop = true;
  bool checkClear = false;
  while (checkLoop) {
    switch (menu) {
      case 1:
        print("Enter n1 : ");
        int n1 = int.parse(stdin.readLineSync()!);
        print("Enter n2 : ");
        int n2 = int.parse(stdin.readLineSync()!);
        print("addition : ");
        print(cal.additionNumber(n1, n2));
        break;
      case 2:
        print("Enter n1 : ");
        int n1 = int.parse(stdin.readLineSync()!);
        print("Enter n2 : ");
        int n2 = int.parse(stdin.readLineSync()!);
        print("subtraction : ");
        print(cal.subtractionNumber(n1, n2));
        break;
      case 3:
        print("Enter n1 : ");
        int n1 = int.parse(stdin.readLineSync()!);
        print("Enter n2 : ");
        int n2 = int.parse(stdin.readLineSync()!);
        print("multiplication : ");
        print(cal.multiplicationNumber(n1, n2));
        break;
      case 4:
        print("Enter n1 : ");
        int n1 = int.parse(stdin.readLineSync()!);
        print("Enter n2 : ");
        int n2 = int.parse(stdin.readLineSync()!);
        print("division : ");
        print(cal.divisionNumber(n1, n2));
        break;
      case 5:
        print("Enter n1 : ");
        int n1 = int.parse(stdin.readLineSync()!);
        print("Enter n2 : ");
        int n2 = int.parse(stdin.readLineSync()!);
        print("division : ");
        print(cal.divisionNumber(n1, n2));
        break;
      case 0:
        cal.clearSum();
        print(cal.getSum());
        break;
      case -1:
        checkLoop = false;
        break;
    }
  }

  // print("addition : ");
  // print(cal.additionNumber());
  // print("subtraction : ");
  // print(cal.subtractionNumber());
  // print("multiplication : ");
  // print(cal.multiplicationNumber());
  // print("division : ");
  // print(cal.divisionNumber());
  // print("division : ");
  // print(cal.divisionNumber());
}
