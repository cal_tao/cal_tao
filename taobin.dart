import 'dart:io';

class Taobin {
  var menu;
  var sw;
  var order;
  var all;

  var drink;
  var swlv;
  var price;

  String getDrink() {
    return this.drink;
  }

  int getPrice() {
    return this.price;
  }

  String getAll() {
    return this.all;
  }

  void showAll() {
    print("Catagory ");
    print("Hot coffee       : press 1");
    print("Iced coffee      : press 2");
    print("Hot milk         : press 3");
    print("Iced milk        : press 4");
    print("Hot tea          : press 5");
    print("Iced tea         : press 6");
    print("Protein shackes  : press 7");
    print("Fruity drinks    : press 8");
    print("Soda             : press 9");
    print("Quit             : press 0");
  }

  String checkAll(int all) {
    switch (all) {
      case 1:
        this.all = "Hot coffee";
        break;
      case 2:
        this.all = "Iced coffee";
        break;
      case 3:
        this.all = "Hot milk";
        break;
      case 4:
        this.all = "Iced milk";
        break;
      case 5:
        this.all = "Hot tea";
        break;
      case 6:
        this.all = "Iced tea";
        break;
      case 7:
        this.all = "Protein shackes";
        break;
      case 8:
        this.all = "Fruity drinks";
        break;
      case 9:
        this.all = "Soda";
        break;
      case 0:
        this.all = "Quit";
        break;
      default:
        break;
    }
    return this.all;
  }

  void showHotCoffee() {
    print("HOT COFFEE : ");
    print("Espresso                 : press 1");
    print("Double espresso          : press 2");
    print("Hot americano            : press 3");
    print("Hot cafe latte           : press 4");
    print("Hot cappuccino           : press 5");
    print("Hot mocha                : press 6");
    print("Hot caramel latte        : press 7");
    print("Milk cafe latte          : press 8");
    print("Hot matcha latte         : press 9");
    print("Hot kokuto cafe latte    : press 10");
    print("Hot thai tea cafe latte  : press 11");
    print("Hot lychee americano     : press 12");
    print("back to ...              : press 0");
  }

  void checkHotCoffee(int menu) {
    switch (menu) {
      case 1:
        print("Espresso");
        this.drink = "Espresso";
        break;
      case 2:
        print("Double espresso");
        this.drink = "Double espresso";
        break;
      case 3:
        print("Hot americano");
        this.drink = "Hot americano";
        break;
      case 4:
        print("Hot cafe latte");
        this.drink = "Hot cafe latte";
        break;
      case 5:
        print("Hot cappuccino");
        this.drink = "Hot cappuccino";
        break;
      case 6:
        print("Hot mocha");
        this.drink = "Hot mocha";
        break;
      case 7:
        print("Hot caramel latte");
        this.drink = "Hot caramel latte";
        break;
      case 8:
        print("Milk cafe latte");
        this.drink = "Milk cafe latte";
        break;
      case 9:
        print("Hot matcha latte");
        this.drink = "Hot matcha latte";
        break;
      case 10:
        print("Hot kokuto cafe latte");
        this.drink = "Hot kokuto cafe latte";
        break;
      case 11:
        print("Hot thai tea cafe latte");
        this.drink = "Hot thai tea cafe latte";
        break;
      case 12:
        print("Hot lychee americano");
        this.drink = "Hot lychee americano";
        break;
      case 0:
        break;
      default:
        print("wrong insert !");
    }
  }

  void showIcedCoffee() {
    print("Iced coffee : ");
    print("Dirty                          : press 1");
    print("Iced espresso                  : press 2");
    print("Iced americano                 : press 3");
    print("Iced cafe latte                : press 4");
    print("Iced cappuccino                : press 5");
    print("Iced mocha                     : press 6");
    print("Iced caramel latte             : press 7");
    print("Iced matcha latte              : press 8");
    print("Iced taiwanese tea cafe latte  : press 9");
    print("Iced kokuto cafe latte         : press 10");
    print("Iced thai tea cafe latte       : press 11");
    print("Iced lychee americano          : press 12");
    print("Iced cannabis americano        : press 13");
    print("back to ...                    : press 0");
  }

  void checkIcedCoffee(int menu) {
    switch (menu) {
      case 1:
        print("Dirty");
        this.drink = "Dirty";
        break;
      case 2:
        print("Iced espresso");
        this.drink = "Iced espresso";
        break;
      case 3:
        print("Iced americano");
        this.drink = "Iced americano";
        break;
      case 4:
        print("Iced cafe latte");
        this.drink = "Iced cafe latte";
        break;
      case 5:
        print("Iced cappuccino");
        this.drink = "Iced cappuccino";
        break;
      case 6:
        print("Iced mocha");
        this.drink = "Iced mocha";
        break;
      case 7:
        print("Iced caramel latte");
        this.drink = "Iced caramel latte";
        break;
      case 8:
        print("Iced matcha latte");
        this.drink = "Iced matcha latte";
        break;
      case 9:
        print("Iced taiwanese tea cafe latte");
        this.drink = "Iced taiwanese tea cafe latte";
        break;
      case 10:
        print("Iced kokuto cafe latte");
        this.drink = "Iced kokuto cafe latte";
        break;
      case 11:
        print("Iced thai tea cafe latte");
        this.drink = "Iced thai tea cafe latte";
        break;
      case 12:
        print("Iced lychee americano");
        this.drink = "Iced lychee americano";
        break;
      case 13:
        print("Iced cannabis americano");
        this.drink = "Iced cannabis americano";
        break;
      case 0:
        break;
      default:
        print("wrong insert !");
    }
  }

  void showHotMilk() {
    print("Hot milk : ");
    print("Hot caramel milk   : press 1");
    print("Hot kokuto milk    : press 2");
    print("Hot cocoa          : press 3");
    print("Hot caramel cocoa  : press 4");
    print("Hot milk           : press 5");
    print("back to ...        : press 0");
  }

  void checkHotMilk(int menu) {
    switch (menu) {
      case 1:
        print("Hot caramel milk");
        this.drink = "Hot caramel milk";
        break;
      case 2:
        print("Hot kokuto milk");
        this.drink = "Hot kokuto milk";
        break;
      case 3:
        print("Hot cocoa");
        this.drink = "Hot cocoa";
        break;
      case 4:
        print("Hot caramel cocoa");
        this.drink = "Hot caramel cocoa";
        break;
      case 5:
        print("Hot milk");
        this.drink = "Hot milk";
        break;
      case 0:
        break;
      default:
        print("wrong insert !");
    }
  }

  void showIcedMilk() {
    print("Iced milk : ");
    print("Iced caramel milk  : press 1");
    print("Iced kokuto milk   : press 2");
    print("Iced cocoa         : press 3");
    print("Iced caramel cocoa : press 4");
    print("Iced pink milk     : press 5");
    print("back to ...        : press 0");
  }

  void checkIcedMilk(int menu) {
    switch (menu) {
      case 1:
        print("Iced caramel milk");
        this.drink = "Iced caramel milk";
        break;
      case 2:
        print("Iced kokuto milk");
        this.drink = "Iced kokuto milk";
        break;
      case 3:
        print("Iced cocoa");
        this.drink = "Iced cocoa";
        break;
      case 4:
        print("Iced caramel cocoa");
        this.drink = "Iced caramel cocoa";
        break;
      case 5:
        print("Iced pink milk");
        this.drink = "Iced pink milk";
        break;
      case 0:
        break;
      default:
        print("wrong insert !");
    }
  }

  void showHotTea() {
    print("Hot tea : ");
    print("Hot chrysanthemum tea  : press 1");
    print("Hot thai milk tea      : press 2");
    print("Hot taiwanese tea      : press 3");
    print("Hot matcha latte       : press 4");
    print("Hot black tea          : press 5");
    print("Hot kokuto tea         : press 6");
    print("Hot lime tea           : press 7");
    print("Hot lychee tea         : press 8");
    print("Hot strawberry tea     : press 9");
    print("Hot blueberry tea      : press 10");
    print("back to ...            : press 0");
  }

  void checkHotTea(int menu) {
    switch (menu) {
      case 1:
        print("Hot chrysanthemum tea");
        this.drink = "Hot chrysanthemum tea";
        break;
      case 2:
        print("Hot thai milk tea");
        this.drink = "Hot thai milk tea";
        break;
      case 3:
        print("Hot taiwanese tea");
        this.drink = "Hot taiwanese tea";
        break;
      case 4:
        print("Hot matcha latte");
        this.drink = "Hot matcha latte";
        break;
      case 5:
        print("Hot black tea");
        this.drink = "Hot black tea";
        break;
      case 6:
        print("Hot kokuto tea");
        this.drink = "Hot kokuto tea";
        break;
      case 7:
        print("Hot lime tea");
        this.drink = "Hot lime tea";
        break;
      case 8:
        print("Hot lychee tea");
        this.drink = "Hot lychee tea";
        break;
      case 9:
        print("Hot strawberry tea");
        this.drink = "Hot strawberry tea";
        break;
      case 10:
        print("Hot blueberry tea");
        this.drink = "Hot blueberry tea";
        break;
      case 0:
        break;
      default:
        print("wrong insert !");
    }
  }

  void showIcedTea() {
    print("Iced tea : ");
    print("Iced chrysanthemum tea : press 1");
    print("Iced thai milk tea     : press 2");
    print("Iced taiwanese tea     : press 3");
    print("Iced matcha latte      : press 4");
    print("Iced tea               : press 5");
    print("Iced kokuto tea        : press 6");
    print("Iced limeade tea       : press 7");
    print("Iced lychee tea        : press 8");
    print("Iced strawberry tea    : press 9");
    print("Iced blueberry tea     : press 10");
    print("back to ...            : press 0");
  }

  void checkIcedTea(int menu) {
    switch (menu) {
      case 1:
        print("Iced chrysanthemum tea");
        this.drink = "Iced chrysanthemum tea";
        break;
      case 2:
        print("Iced thai milk tea");
        this.drink = "Iced thai milk tea";
        break;
      case 3:
        print("Iced taiwanese tea");
        this.drink = "Iced taiwanese tea";
        break;
      case 4:
        print("Iced matcha latte");
        this.drink = "Iced matcha latte";
        break;
      case 5:
        print("Iced tea");
        this.drink = "Iced tea";
        break;
      case 6:
        print("Iced kokuto tea");
        this.drink = "Iced kokuto tea";
        break;
      case 7:
        print("Iced limeade tea");
        this.drink = "Iced limeade tea";
        break;
      case 8:
        print("Iced lychee tea");
        this.drink = "Iced lychee tea";
        break;
      case 9:
        print("Iced strawberry tea");
        this.drink = "Iced strawberry tea";
        break;
      case 10:
        print("Iced blueberry tea");
        this.drink = "Iced blueberry tea";
        break;
      case 0:
        break;
      default:
        print("wrong insert !");
    }
  }

  void showProteinShackes() {
    print("Protein shackes : ");
    print("Matcha protein shackes         : press 1");
    print("Chocolate protein shackes      : press 2");
    print("Strawberry protein shackes     : press 3");
    print("Espresso protein shackes       : press 4");
    print("Thai tea protein shackes       : press 5");
    print("Brown sugar protein shackes    : press 6");
    print("Taiwanese tea protein shackes  : press 7");
    print("Caramel protein shackes        : press 8");
    print("Plaun protein shackes          : press 9");
    print("milk shackes                   : press 10");
    print("back to ...                    : press 0");
  }

  void checkProteinShackes(int menu) {
    switch (menu) {
      case 1:
        print("Matcha protein shackes");
        this.drink = "Matcha protein shackes";
        break;
      case 2:
        print("Chocolate protein shackes");
        this.drink = "Chocolate protein shackes";
        break;
      case 3:
        print("Strawberry protein shackes");
        this.drink = "Strawberry protein shackes";
        break;
      case 4:
        print("Espresso protein shackes");
        this.drink = "Espresso protein shackes";
        break;
      case 5:
        print("Thai tea protein shackes");
        this.drink = "Thai tea protein shackes";
        break;
      case 6:
        print("Brown sugar protein shackes");
        this.drink = "Brown sugar protein shackes";
        break;
      case 7:
        print("Taiwanese tea protein shackes");
        this.drink = "Taiwanese tea protein shackes";
        break;
      case 8:
        print("Caramel protein shackes");
        this.drink = "Caramel protein shackes";
        break;
      case 9:
        print("Plaun protein shackes");
        this.drink = "Plaun protein shackes";
        break;
      case 10:
        print("milk shackes");
        this.drink = "milk shackes";
        break;
      case 0:
        break;
      default:
        print("wrong insert !");
    }
  }

  void showFruityDrinks() {
    print("Fruity drinks : ");
    print("Hot limeade        : press 1");
    print("Iced limeade       : press 2");
    print("Iced lychee        : press 3");
    print("Iced strawberry    : press 4");
    print("Ice blueberry      : press 5");
    print("Iced plum          : press 6");
    print("Iced mango         : press 7");
    print("Iced sala          : press 8");
    print("Iced limeade sala  : press 9");
    print("back to ...        : press 0");
  }

  void checkFruityDrinks(int menu) {
    switch (menu) {
      case 1:
        print("Hot limeade");
        this.drink = "Hot limeade";
        break;
      case 2:
        print("Iced limeade");
        this.drink = "Iced limeade";
        break;
      case 3:
        print("Iced lychee");
        this.drink = "Iced lychee";
        break;
      case 4:
        print("Iced strawberry");
        this.drink = "Iced strawberry";
        break;
      case 5:
        print("Ice blueberry");
        this.drink = "Ice blueberry";
        break;
      case 6:
        print("Iced plum");
        this.drink = "Iced plum";
        break;
      case 7:
        print("Iced mango");
        this.drink = "Iced mango";
        break;
      case 8:
        print("Iced sala");
        this.drink = "Iced sala";
        break;
      case 9:
        print("Iced limeade sala");
        this.drink = "Iced limeade sala";
        break;
      case 0:
        break;
      default:
        print("wrong insert !");
    }
  }

  void showSoda() {
    print("Soda : ");
    print("Pepsi                : press 1");
    print("Iced limenade soda   : press 2");
    print("Iced lychee soda     : press 3");
    print("Iced strawberry      : press 4");
    print("Iced cannabis soda   : press 5");
    print("Iced plum soda       : press 6");
    print("Iced ginger soda     : press 7");
    print("Iced blueberry soda  : press 8");
    print("Iced sala soda       : press 9");
    print("Iced lime sala soda  : press 10");
    print("back to ...          : press 0");
  }

  void checkSoda(int menu) {
    switch (menu) {
      case 1:
        print("Pepsi");
        this.drink = "Pepsi";
        break;
      case 2:
        print("Iced limenade soda");
        this.drink = "Iced limenade soda";
        break;
      case 3:
        print("Iced lychee soda");
        this.drink = "Iced lychee soda";
        break;
      case 4:
        print("Iced strawberry");
        this.drink = "Iced strawberry";
        break;
      case 5:
        print("Iced cannabis soda");
        this.drink = "Iced cannabis soda";
        break;
      case 6:
        print("Iced plum soda");
        this.drink = "Iced plum soda";
        break;
      case 7:
        print("Iced ginger soda");
        this.drink = "Iced ginger soda";
        break;
      case 8:
        print("Iced blueberry soda");
        this.drink = "Iced blueberry soda";
        break;
      case 9:
        print("Iced sala soda");
        this.drink = "Iced sala soda";
        break;
      case 10:
        print("Iced lime sala soda");
        this.drink = "Iced lime sala soda";
        break;
      case 0:
        break;
      default:
        print("wrong insert !");
    }
  }

  void showMenu() {
    switch (all) {
      case "Hot coffee":
        showHotCoffee();
        break;
      case "Iced coffee":
        showIcedCoffee();
        break;
      case "Hot milk":
        showHotMilk();
        break;
      case "Iced milk":
        showIcedMilk();
        break;
      case "Hot tea":
        showHotTea();
        break;
      case "Iced tea":
        showIcedTea();
        break;
      case "Protein shackes":
        showProteinShackes();
        break;
      case "Fruity drinks":
        showFruityDrinks();
        break;
      case "Soda":
        showSoda();
        break;
      case "Quit":
        break;
      default:
        print("wrong insert !");
        break;
    }
  }

  void showSweetLevel() {
    print("1.No Sugar  2.Less Sweet  3.Just right  4.Sweet  5.Very Sweet");
  }

  void checkMenu(int menu) {
    switch (all) {
      case "Hot coffee":
        checkHotCoffee(menu);
        break;
      case "Iced coffee":
        checkIcedCoffee(menu);
        break;
      case "Hot milk":
        checkHotMilk(menu);
        break;
      case "Iced milk":
        checkIcedMilk(menu);
        break;
      case "Hot tea":
        checkHotTea(menu);
        break;
      case "Iced tea":
        checkIcedTea(menu);
        break;
      case "Protein shackes":
        checkProteinShackes(menu);
        break;
      case "Fruity drinks":
        checkFruityDrinks(menu);
        break;
      case "Soda":
        checkSoda(menu);
        break;
      case "Quit":
        break;
      default:
        print("wrong insert !");
        break;
    }
  }

  void sweetlv(var sw) {
    switch (sw) {
      case 1:
        print("No sugar");
        swlv = "No sugar";
        break;
      case 2:
        print("Less Sweet");
        swlv = "Less Sweet";
        break;
      case 3:
        print("Just right");
        swlv = "Just right";
        break;
      case 4:
        print("Sweet");
        swlv = "Sweet";
        break;
      case 5:
        print("Very Sweet");
        swlv = "Very Sweet";
        break;
      default:
        print("wrong insert !");
        break;
    }
  }

  void checkPrice(String drink) {
    switch (drink) {
      case "Espresso":
        this.price = 25;
        break;
    }
  }

  void showProduct() {
    print(">>> " + this.drink);
    print("sweet level : " + this.swlv);
    print("total : ");
    print(this.price);
  }

  void showOrderMenu() {
    print("You want to order menu ? (y/n)");
  }
}

void main() {
  Taobin tb = new Taobin();
  var menu;
  var sw;
  bool checkLoop = false;
  var order;
  var all;

  while (!checkLoop) {
    tb.showOrderMenu();
    order = stdin.readLineSync()!;
    if (order != 'y') {
      if (order != 'n') {
        print('You insert wrong input !!!');
      } else {
        print("Thank you !");
        break;
      }
    } else {
      tb.showAll();
      print("Enter number of menu : ");
      all = int.parse(stdin.readLineSync()!);
      tb.checkAll(all);
      tb.showMenu();
      print("Enter number of menu : ");
      menu = int.parse(stdin.readLineSync()!);
      tb.checkMenu(menu);
      tb.showSweetLevel();
      print("Enter number of sweetness level : ");
      sw = int.parse(stdin.readLineSync()!);
      tb.sweetlv(sw);
      tb.checkPrice(tb.getDrink());
      tb.showProduct();
    }
  }
}
